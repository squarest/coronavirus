package com.vfstudio.coronavirus.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.vfstudio.coronavirus.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitUtils private constructor() {
    val apiRetrofit: Retrofit

    init {
        apiRetrofit = provideRetrofit(BuildConfig.API_BASE_URL)
    }

    private fun provideRetrofit(baseUrl: String): Retrofit {
        return Retrofit.Builder().baseUrl(baseUrl)
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    fun provideClient(): ApiService {
        return apiRetrofit.create(ApiService::class.java)
    }
    companion object {
        private const val CONNECT_TIMEOUT = 30
        private const val WRITE_TIMEOUT = 120
        private const val TIMEOUT = 30
        var instance: RetrofitUtils? = null
            get() {
                if (field == null) field =
                    RetrofitUtils()
                return field
            }
            private set
        var gson: Gson? = null
            get() {
                if (field == null) field =
                    GsonBuilder().create()
                return field
            }
            private set

        fun provideOkHttpClient(): OkHttpClient {
            return provideOkHttpBuilder().build()
        }

        private fun provideOkHttpBuilder(): OkHttpClient.Builder {
            return OkHttpClient.Builder()
                .connectTimeout(
                    CONNECT_TIMEOUT.toLong(),
                    TimeUnit.SECONDS
                )
                .writeTimeout(
                    WRITE_TIMEOUT.toLong(),
                    TimeUnit.SECONDS
                )
                .readTimeout(
                    TIMEOUT.toLong(),
                    TimeUnit.SECONDS
                )
                .addInterceptor(provideHttpLoggingInterceptor())
        }

        private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            return httpLoggingInterceptor
        }
    }
}