package com.vfstudio.coronavirus.api

import com.vfstudio.coronavirus.models.NewsResponse
import com.vfstudio.coronavirus.models.Stats
import com.vfstudio.coronavirus.models.StatsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {
    @get:GET("all")
    val getStats: Single<StatsResponse>?

    @Headers(value = ["Authorization: GejMz3K-GPyVO8wsNexAwUWKttAY6JvdsY16KL3QpbbwyGVg"])
    @GET("search")
    fun getNews(
        @Query("keywords") keywords: String,
        @Query("language") language: String
    ): Single<NewsResponse>

    @get:GET("confirmed")
    val getConfirmed: Single<Stats>?

    @get:GET("deaths")
    val getDeaths: Single<Stats>?

    @get:GET("recovered")
    val getRecovered: Single<Stats>?
}