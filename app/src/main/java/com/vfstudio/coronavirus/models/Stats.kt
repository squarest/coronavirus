package com.vfstudio.coronavirus.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Stats :Serializable {
    @SerializedName("latest")
    var latest: Int? = null

    @SerializedName("locations")
    var locations: List<Location>? = null

}