package com.vfstudio.coronavirus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Location : Serializable {
//    @SerializedName("coordinates")
//    var coordinates: Coordinates? = null
    @SerializedName("country")
    var country: String? = null
    @SerializedName("latest")
    var latest: Int? = null
    @SerializedName("province")
    var province: String? = null

}