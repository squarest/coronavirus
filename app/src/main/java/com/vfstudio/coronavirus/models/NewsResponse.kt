package com.vfstudio.coronavirus.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class NewsResponse(
    @SerializedName("status")
    var status: String? = null,

    @SerializedName("news")
    var news: List<News>? = null,

    @SerializedName("page")
    var page: Int? = null
)


data class News(
//    @SerializedName("id")
//    var id: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("description")
    var description: String? = null,

    @SerializedName("url")
    var url: String? = null,

//    @SerializedName("author")
//    var author: String? = null,

    @SerializedName("image")
    var image: String? = null,

    @SerializedName("language")
    var language: String? = null,
//
//    @SerializedName("category")
//    var category: List<String>? = null,

    @SerializedName("published")
    var published: String? = null
)