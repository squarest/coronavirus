package com.vfstudio.coronavirus.models

import com.vfstudio.coronavirus.R

enum class StatsType(val title: Int, val subtitle: Int, val color: Int) {
    CASES(
        R.string.confirmed_cases,
        R.string.confirmed_cases_short,
        R.color.red
    ),
    DEATHS(
        R.string.deaths,
        R.string.deaths,
        R.color.white
    ),
    RECOVERED(
        R.string.recovered,
        R.string.recovered,
        R.color.blue_green
    )

}