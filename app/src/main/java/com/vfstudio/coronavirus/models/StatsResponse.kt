package com.vfstudio.coronavirus.models

import com.google.gson.annotations.SerializedName

class StatsResponse {
    @SerializedName("confirmed")
    var confirmedStats: Stats? = null
    @SerializedName("deaths")
    var deathsStats: Stats? = null
    @SerializedName("recovered")
    var recoveredStats: Stats? = null
}