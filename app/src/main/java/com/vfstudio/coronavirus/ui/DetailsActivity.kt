package com.vfstudio.coronavirus.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.vfstudio.coronavirus.R
import com.vfstudio.coronavirus.api.ApiService
import com.vfstudio.coronavirus.api.RetrofitUtils
import com.vfstudio.coronavirus.models.Stats
import com.vfstudio.coronavirus.models.StatsType
import com.vfstudio.coronavirus.ui.adapters.LocationsAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_details.*
import java.util.*

class DetailsActivity : AppCompatActivity() {
    companion object {
        const val STATS_ARG = "stats_arg"
        const val STATS_TYPE_ARG = "stats_arg_type"
    }

    private lateinit var statsType: StatsType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        statsType = intent.getSerializableExtra(STATS_TYPE_ARG) as StatsType
        back_button.setOnClickListener { onBackPressed() }
        loadData()
    }

    private fun setStats(stats: Stats) {
        info_card.hideDetailsButton()
        info_card.setValue(stats.latest!!)
        info_card.setType(statsType)
        locations_list.layoutManager = LinearLayoutManager(this)
        locations_list.adapter =
            LocationsAdapter(
                stats.locations,
                statsType
            )

    }

    private fun loadData() {
        val apiService = RetrofitUtils.instance?.provideClient()
        val request = when (statsType) {
            StatsType.CASES -> apiService?.getConfirmed;
            StatsType.DEATHS -> apiService?.getDeaths;
            else -> apiService?.getRecovered
        }
        request?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnSubscribe { showLoading() }
            ?.doFinally { hideLoading() }
            ?.doOnSuccess { stats ->
                stats.locations = stats.locations?.sortedWith(compareByDescending { it.latest })
            }
            ?.subscribe(
                { statsResponse -> setStats(statsResponse) },
                { throwable -> throwable.printStackTrace() })
    }

    private fun showLoading() {
        content_container.visibility = View.GONE
        progress_bar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        content_container.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE
    }
}