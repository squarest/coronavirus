package com.vfstudio.coronavirus.ui.adapters

interface OnItemClickListener {
    fun onItemClick(newsUrl: String)
}