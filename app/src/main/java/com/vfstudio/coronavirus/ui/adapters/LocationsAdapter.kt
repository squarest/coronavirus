package com.vfstudio.coronavirus.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vfstudio.coronavirus.R
import com.vfstudio.coronavirus.models.Location
import com.vfstudio.coronavirus.models.StatsType
import kotlinx.android.synthetic.main.item_country.view.*
import java.text.NumberFormat

class LocationsAdapter(private val items: List<Location>?, val statsType: StatsType) :
    RecyclerView.Adapter<ViewHolder>() {

    lateinit var context: Context
    override fun getItemCount(): Int {
        return items!!.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_country,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items!![position]
        holder.countryName.text = item.country
        holder.counter.text = NumberFormat.getInstance().format(item.latest)
        holder.counter.setTextColor(context.resources.getColor(statsType.color))
        holder.subtitle.text = context.resources.getString(statsType.subtitle)
        if (!item.province.isNullOrEmpty()) {
            holder.provinceName.text = item.province
            holder.provinceName.visibility = View.VISIBLE
        } else {
            holder.provinceName.visibility = View.GONE
        }
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val countryName = view.country_name
    val counter = view.counter
    val subtitle = view.counter_subtitle
    val provinceName = view.province_name

}
