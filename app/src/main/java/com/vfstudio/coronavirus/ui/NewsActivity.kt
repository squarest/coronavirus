package com.vfstudio.coronavirus.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import com.vfstudio.coronavirus.R
import com.vfstudio.coronavirus.api.ApiClient
import com.vfstudio.coronavirus.models.News
import com.vfstudio.coronavirus.ui.adapters.NewsAdapter
import com.vfstudio.coronavirus.ui.adapters.OnItemClickListener
import com.vfstudio.coronavirus.ui.views.NewsDetailsActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_news.*

class NewsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        prepareUI()
        loadData()
    }


    private fun prepareUI() {
        news_list.layoutManager = LinearLayoutManager(this)
        back_button.setOnClickListener { this.onBackPressed() }
    }

    @SuppressLint("CheckResult")
    private fun loadData() {
        ApiClient.getInstanceNews().getNews("коронавирус", "ru")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doFinally { hideLoading() }
            .subscribe(
                { statsResponse ->
                    if (statsResponse.status == "ok"
                        && statsResponse.news?.isNotEmpty()!!
                        && statsResponse.news != null
                    ) {
                        initRecyclerView(statsResponse.news!!)
                    }
                }, { throwable -> throwable.printStackTrace() })
    }

    private fun initRecyclerView(newsList: List<News>) {
        news_list.adapter =
            NewsAdapter(
                newsList,
                object : OnItemClickListener {
                    override fun onItemClick(newsUrl: String) {
                        openNewsDetailsActivity(newsUrl)
                    }
                }
            )
    }

    private fun openNewsDetailsActivity(newsUrl: String) {
        val intent = Intent(this, NewsDetailsActivity::class.java)
        intent.putExtra(NewsDetailsActivity.NEWS_UTL, newsUrl)
        startActivity(intent)
    }


    private fun showLoading() {
        news_progress_bar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        news_progress_bar.visibility = View.GONE
    }
}
