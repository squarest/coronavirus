package com.vfstudio.coronavirus.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.vfstudio.coronavirus.R
import com.vfstudio.coronavirus.models.News
import kotlinx.android.synthetic.main.item_news.view.*

class NewsAdapter(
    private val items: List<News>,
    val onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        return NewsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_news,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val item = items[position]
        holder.newsTitle.text = item.title
        holder.newsDescription.text = item.description
        holder.setImage(item.image!!)
        holder.newsContainer.setOnClickListener { onItemClickListener.onItemClick(item.url!!) }
    }


    inner class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val newsTitle = view.news_title
        val newsDescription = view.news_description
        val newsImg = view.news_img
        val newsContainer = view.news_container

        fun setImage(url: String) {
            Glide.with(itemView)
                .load(url)
                .apply(
                    RequestOptions()
//                        .error(R.color.item_fit_light)
                        .placeholder(android.R.color.darker_gray)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .priority(Priority.HIGH)
                )
                .into(newsImg)
        }

    }
}