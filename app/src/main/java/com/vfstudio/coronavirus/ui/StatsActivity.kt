package com.vfstudio.coronavirus.ui

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.appodeal.ads.Appodeal
import com.vfstudio.coronavirus.R
import com.vfstudio.coronavirus.api.ApiClient
import com.vfstudio.coronavirus.models.StatsResponse
import com.vfstudio.coronavirus.models.StatsType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class StatsActivity : AppCompatActivity() {

    private lateinit var statsResponse: StatsResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAd()
        prepareUI()
        loadData()
    }

    private fun initAd() {
        Appodeal.initialize(
            this,
            "3a9dadc6e064d536709578335e0e25d69a465fadcdebd45f",
            Appodeal.BANNER,
            false
        )
        Appodeal.setBannerViewId(R.id.ad);
        Appodeal.show(this, Appodeal.BANNER_VIEW);
    }

    private fun prepareUI() {
        confirmed_card.setType(StatsType.CASES)
        died_card.setType(StatsType.DEATHS)
        recovered_card.setType(StatsType.RECOVERED)

        news_img.setOnClickListener {
            openFeedScreen()
        }
        confirmed_card.setOnClickListener { openDetailsScreen(StatsType.CASES) }
        died_card.setOnClickListener { openDetailsScreen(StatsType.DEATHS) }
        recovered_card.setOnClickListener { openDetailsScreen(StatsType.RECOVERED) }
        info_button.setOnClickListener { showInfoPopup() }

    }

    @SuppressLint("CheckResult")
    private fun loadData() {
        ApiClient.getInstanceStats().getStats
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnSubscribe { showLoading() }
            ?.doFinally { hideLoading() }
            ?.subscribe(
                { statsResponse -> setStats(statsResponse) },
                { throwable -> throwable.printStackTrace() })

    }

    private fun setStats(statsResponse: StatsResponse) {
        this.statsResponse = statsResponse
        confirmed_card.setValue(statsResponse.confirmedStats?.latest!!)
        died_card.setValue(statsResponse.deathsStats?.latest!!)
        recovered_card.setValue(statsResponse.recoveredStats?.latest!!)
    }

    private fun showLoading() {
        content_container.visibility = View.GONE
        progress_bar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        content_container.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE
    }

    private fun openFeedScreen() {
        val intent = Intent(this, NewsActivity::class.java)
        startActivity(intent)
    }

    private fun openDetailsScreen(statsType: StatsType) {
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.STATS_TYPE_ARG, statsType)
        startActivity(intent)
    }

    private fun showInfoPopup() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.info_title))
            .setMessage(getString(R.string.info_text))
            .setPositiveButton("I understand") { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }
}
