package com.vfstudio.coronavirus.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.cardview.widget.CardView
import com.vfstudio.coronavirus.R
import com.vfstudio.coronavirus.models.StatsType
import kotlinx.android.synthetic.main.counter_view.view.*
import java.text.NumberFormat

class CounterView : CardView {
    constructor(context: Context) : super(context) {
        initLayout()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initLayout()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initLayout()
    }

    private fun initLayout() {
        View.inflate(context, R.layout.counter_view, this)
        setCardBackgroundColor(resources.getColor(R.color.gray))
        radius = 16f
        cardElevation = 8f
    }

    fun setType(statsType: StatsType) {
        setTitleText(resources.getString(statsType.title))
        setTitleColor(resources.getColor(statsType.color))
    }

    fun hideDetailsButton() {
        details.visibility = GONE
        details_arrow.visibility = GONE
    }

    fun setTitleColor(color: Int) {
        value.setTextColor(color)
    }

    fun setTitleText(text: CharSequence) {
        title.text = text
    }

    fun setValue(valueInt: Int) {
        value.text = NumberFormat.getInstance().format(valueInt)
    }


}