package com.vfstudio.coronavirus.ui.views

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vfstudio.coronavirus.R
import com.vfstudio.coronavirus.utils.CustomWebViewClient
import kotlinx.android.synthetic.main.activity_news_details.*

class NewsDetailsActivity : AppCompatActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)
        val newsUrl = intent.getStringExtra(NEWS_UTL)

        news_web.settings.javaScriptEnabled = true
        news_web.webViewClient = CustomWebViewClient
        news_web.loadUrl(newsUrl)
    }

    override fun onBackPressed() {
        if (news_web.canGoBack())
            news_web.goBack();
        else
            super.onBackPressed();
    }

    companion object {
        const val NEWS_UTL = "news_url"
    }
}
