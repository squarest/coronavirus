package com.vfstudio.coronavirus

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.vfstudio.coronavirus.ui.StatsActivity
import java.util.*


class NotificationsService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        remoteMessage.notification?.let {
            showNotification(remoteMessage.notification!!.title, remoteMessage.notification!!.body)
        }

    }
    private fun showNotification(title: String?, text: String?) {
        val intent = Intent(this, StatsActivity::class.java)
        val pIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val n: Notification = Notification.Builder(this)
            .setContentTitle(title)
            .setContentText(text)
            .setContentIntent(pIntent)
            .setAutoCancel(true)
            .build()


        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(Random().nextInt(60000), n)
    }
}